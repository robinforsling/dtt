# Description

This folder contains MATLAB implementations for the robust optimization parts of the evaluation examples of the following paper:

"Conservative Linear Unbiased Estimation Under Partially Known Covariances", IEEE Transactions on Signal Processing, vol. 70, pp. 3123-3135, Jun. 2022.

**System requirements**:

* MATLAB
* YALMIP optimization toolbox: https://yalmip.github.io
* MOSEK solver: https://www.mosek.com
