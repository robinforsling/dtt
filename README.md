# Decentralized Target Tracking

This repository contains files and examples related to *decentralized target tracking*. The repository is under development. See below for files and examples related to specific scientific papers.

Remember to add paths, by for instance using: ``paths()``  or ``paths(true)``. To remove added paths use: ``paths(false)``. Note, this is *not* necessary for the standalone examples. 

## Common Information Estimate (CIE)

A standalone example of the *common information estimate* defined in the paper

"Decentralized Data Fusion of Dimension-Reduced Estimates Using Local Information Only", IEEE Aerospace Conference 2023.

is available in the folder:`/cie`

## Conservative Linear Unbiased Estimation (CLUE)

For code and examples related to

"Conservative Linear Unbiased Estimation Under Partially Known Covariances", *IEEE Transactions on Signal Processing*, vol. 70, pp. 3123-3135, Jun. 2022.

see folder: `/clue`

## Generalized Eigenvalue Optimization (GEVO)

For code and examples related to 

"Decentralized State Estimation In A Dimension-Reduced Linear Regression", submitted to *IEEE Transactions on Signal Processing*, Oct. 2022. 

see folder: `/gevo`
