function [Psi,varargout] = gevo_kf(R1,R2,H,m)
% --- gevo_kf() -----------------------------------------------------------
% The GEVO method for Kalman fuser loss function.
%
% 2022-09-29 Robin Forsling

Q = H*R1*R1*H';
S = H*R1*H'+R2; 
[X,D] = eig(Q,S,'qz');
idx_vec = get_max_idx_vec(D,m);
Psi = gram_schmidt_process(X(:,idx_vec))';

if nargout > 1; varargout{1} = kf_loss_function(R1,R2,H,Psi); end 

end

function J = kf_loss_function(R1,R2,H,Psi)
    A = Psi*H*R1;
    B = Psi*(H*R1*H'+R2)*Psi';
    J = trace(R1 - A'/B*A);
end