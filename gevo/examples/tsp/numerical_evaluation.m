function varargout = numerical_evaluation
% --- numerical_evaluation() ----------------------------------------------
% Numerical evaluation example of the paper:
% "Decentralized State Estimation In A Dimension-Reduced Linear Regression"
%
% 2022-10-04 Robin Forsling

set_latex_interpreter;

n = 6;
H = eye(n);
Ai = diag([64 32 16 8 4 2]);
Bi = diag([5 8 13 21 34 55]);
Ci = [  16 4 4 0 -2 0 ; ...
        4 20 8 -8 -4 -4 ; ...
        4 8 30 0 -4 -4 ; ...
        0 -8 0 50 0 0 ; ...
        -2 -4 -4 0 10 0 ; ...
        0 -4 -4 0 0 20];

m_vec = [1 2 3];
nm = length(m_vec);

rho_vec = 0.01:0.02:0.99;
nrho = length(rho_vec);

bc_J = zeros(nm,nrho); dkf_J = bc_J; ci_J = bc_J; le_J = bc_J; nkf_J = bc_J;
s = zeros(nm,nrho); dkf_c.epsilon = s; dkf_c.zeta = s; dkf_c.b = s; dkf_c.lvl = s; 
ci_c = dkf_c; le_c = dkf_c; nkf_c = dkf_c;

for i = 1:nm

    m = m_vec(i);

    for j = 1:nrho
    
        rho = rho_vec(j);

        Gi = rho*Ci;
        R1 = inv((1-rho)*Ai + Gi); 
        R2 = inv((1-rho)*Bi + Gi);
        R2d = inv((1-rho)*Bi);
        R12 = R1*Gi*R2; 
        R12d = zeros(n);       
    
        % GEVO
        [~,J_bc] = gevo_bsc(R1,R2,R12,H,m);
        [Psi_dkf,J_dkf] = gevo_kf(R1,R2d,H,m);
        [Psi_ci,J_ci] = gevo_ci(R1,R2,H,m);
        [Psi_le,J_le] = gevo_le(R1,R2,H,m);
        [Psi_nkf,J_nkf] = gevo_kf(R1,R2,H,m);
    
        dkf_J(i,j) = J_dkf/J_bc;
        ci_J(i,j) = J_ci/J_bc;
        le_J(i,j) = J_le/J_bc;
        nkf_J(i,j) = J_nkf/J_bc;
    
        [Pdkf,Pdkf0] = kf_true(R1,R2d,R12d,Psi_dkf);
        [Pci,Pci0] = ci_true(R1,R2,R12,Psi_ci);
        [Ple,Ple0] = le_true(R1,R2,R12,Psi_le);
        [Pnkf,Pnkf0] = kf_true(R1,R2,R12,Psi_nkf);

        c_dkf = check_conservative(Pdkf,Pdkf0);
        c_ci = check_conservative(Pci,Pci0);
        c_le = check_conservative(Ple,Ple0);
        c_nkf = check_conservative(Pnkf,Pnkf0);

        dkf_c.epsilon(i,j) = c_dkf.anees; dkf_c.zeta(i,j) = c_dkf.max_eig; 
        ci_c.epsilon(i,j) = c_ci.anees; ci_c.zeta(i,j) = c_ci.max_eig; 
        le_c.epsilon(i,j) = c_le.anees; le_c.zeta(i,j) = c_le.max_eig; 
        nkf_c.epsilon(i,j) = c_nkf.anees; nkf_c.zeta(i,j) = c_nkf.max_eig;
    end
end

data.dkf.J = dkf_J; data.dkf.epsilon = dkf_c.epsilon; data.dkf.zeta = dkf_c.zeta;
data.ci.J = ci_J; data.ci.epsilon = ci_c.epsilon; data.ci.zeta = ci_c.zeta;
data.le.J = le_J; data.le.epsilon = le_c.epsilon; data.le.zeta = le_c.zeta;
data.nkf.J = nkf_J; data.nkf.epsilon = nkf_c.epsilon; data.nkf.zeta = nkf_c.zeta;

if nargout > 0; varargout{1} = data; end


% --- PLOTTING ------------------------------------------------------------
figure(2);clf

clr = get_colors_rgb;
clr2 = get_colors_rgb('cb-div-spectral');

clrdkf = clr.blue;
clrci = clr.green*0.8;
clrle = clr2.lightturkos;
clrnkf = clr.orange;

lw_vec = 1.0:0.5:4;
ls = {'-','--',':','-.','-','--','-.',':'};


% Loss function:
subplot(10,1,1:4); hold on
 
for i = 1:nm; h = plot(rho_vec,dkf_J(i,:),'-','DisplayName',sprintf('dKF $m=%d$',m_vec(i))); set_h(h,clrdkf,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,ci_J(i,:),'-','DisplayName',sprintf('CI $m=%d$',m_vec(i))); set_h(h,clrci,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,le_J(i,:),'-','DisplayName',sprintf('LE $m=%d$',m_vec(i))); set_h(h,clrle,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,nkf_J(i,:),'-','DisplayName',sprintf('nKF $m=%d$',m_vec(i))); set_h(h,clrnkf,lw_vec(i),ls{i}); end

xlim([0 1]); ylim([0.6 2.0])
legend('show','Location','NorthOutside','NumColumns',4); 
set(gca,'xtick',0:0.2:1.0)
set(gca,'xticklabel',[])
box on; grid on
ylabel('$J$','interpreter','latex'); 

set_latex_interpreter

% ANEES:    
subplot(10,1,5:7); hold on

for i = 1:nm; h = plot(rho_vec,dkf_c.epsilon(i,:),'-','DisplayName',sprintf('dKF $m=%d$',m_vec(i))); set_h(h,clrdkf,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,ci_c.epsilon(i,:),'-','DisplayName',sprintf('CI $m=%d$',m_vec(i))); set_h(h,clrci,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,le_c.epsilon(i,:),'-','DisplayName',sprintf('LE $m=%d$',m_vec(i))); set_h(h,clrle,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,nkf_c.epsilon(i,:),'-','DisplayName',sprintf('nKF $m=%d$',m_vec(i))); set_h(h,clrnkf,lw_vec(i),ls{i}); end

xlim([0 1]); ylim([0.5 1.5])
box on; grid on

set(gca,'xtick',0:0.2:1.0)
set(gca,'xticklabel',[])
ylabel('$\varepsilon$','interpreter','latex');  


% MAX EIG:
subplot(10,1,8:10); hold on

for i = 1:nm; h = plot(rho_vec,dkf_c.zeta(i,:),'-','DisplayName',sprintf('dKF $m=%d$',m_vec(i))); set_h(h,clrdkf,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,ci_c.zeta(i,:),'-','DisplayName',sprintf('CI $m=%d$',m_vec(i))); set_h(h,clrci,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,le_c.zeta(i,:),'-','DisplayName',sprintf('LE $m=%d$',m_vec(i))); set_h(h,clrle,lw_vec(i),ls{i}); end
for i = 1:nm; h = plot(rho_vec,nkf_c.zeta(i,:),'-','DisplayName',sprintf('nKF $m=%d$',m_vec(i))); set_h(h,clrnkf,lw_vec(i),ls{i}); end

xlim([0 1]);
box on; grid on
set(gca,'xtick',0:0.2:1.0)
xlabel('$\rho$','interpreter','latex'); 
ylabel('$\eta$','interpreter','latex');  

end

function [P,P0] = kf_true(R1,R2,R12,Psi)
    RPsi = Psi*R2*Psi';
    R12Psi = R12*Psi';
    R = [R1 R12Psi ; R12Psi' RPsi];
    P = inv(inv(R1) + Psi'/RPsi*Psi);
    K1 = P/R1; K2 = P*Psi'/RPsi;
    K = [K1 K2];
    P0 = K*R*K';
end

function [P,P0] = ci_true(R1,R2,R12,Psi)
    RPsi = Psi*R2*Psi';
    R12Psi = R12*Psi';
    f = @(w) trace(inv(w*inv(R1) + (1-w)*Psi'/RPsi*Psi));
    w = fminbnd(f,0,1);
    P = inv(w*inv(R1) + (1-w)*Psi'/RPsi*Psi);
    R0 = [R1 R12Psi ; R12Psi' RPsi];
    K1 = w*P/R1; K2 = (1-w)*P*Psi'/RPsi;
    K = [K1 K2];
    P0 = K*R0*K';
end

function [P,P0] = le_true(R1,R2,R12,Psi)
    n = size(R1,1);
    RPsi = Psi*R2*Psi'; R12Psi = R12*Psi';
    R = [R1 R12Psi ; R12Psi' RPsi];
    I1 = inv(R1); IPsi = inv(RPsi); I2 = Psi'*IPsi*Psi;
    [U1,D1] = eig(make_symmetric(I1)); T1 = inv(sqrtm(D1))*U1';
    [U2,D2] = eig(make_symmetric(T1*I2*T1')); T = U2'*T1; Ti = inv(T);
    P = inv(Ti*diag(max([ones(1,n) ; diag(D2)']))*Ti'); 
    Gammai = Ti*diag(min([ones(1,n) ; diag(D2)']))*Ti';
    R12 = R1*Gammai*R2;
    S = R1+R2-R12-R12';
    K2 = (R1-R12)*Psi'/(Psi*S*Psi'); 
    K1 = eye(n) - K2*Psi;
    K = [K1 K2];
    P0 = K*R*K';
end

function c = check_conservative(P,P0)
    P = make_symmetric(P);
    n = size(P,1);
    c.anees = trace(P0/P)/n;
    L = chol(P,'lower'); Li = inv(L);
    c.max_eig = max(eig(make_symmetric(Li*P0*Li')));
end

function set_h(h,clr,lw,ls)
    h.Color = clr; 
    h.LineWidth = lw; 
    h.LineStyle = ls;
end