function gevo_communication_reduction
% --- gevo_communication_reduction() --------------------------------------
% Communication reduction example of the paper:
% "Decentralized State Estimation In A Dimension-Reduced Linear Regression"
%
% 2022-10-04 Robin Forsling

set_latex_interpreter;

f = @(m,p) (2*m*p-m^2+3*m) ./ (p.*(p+3));

m_max = 9;
p_vec = cell(m_max,1);
p_vec{1} = 2:20; 
for m = 2:m_max; p_vec{m} = p_vec{m-1}(2:end); end

% Plot:
figure(2);clf;hold on

c = get_colors_rgb;
c = [c.red;c.yellow;c.green;c.blue;c.purple;c.cyan;c.orange];
if m_max > 7; c = [c;linspace(0,0.5,m_max-7)'*[1 1 1]]; end

for m = 1:m_max
    h = plot(p_vec{m},f(m,p_vec{m}),'DisplayName',sprintf('$m=%d$',m)); update_handle(h,c(m,:));
end

xlim([p_vec{1}(1)-1 p_vec{1}(end)+1])
ylim([0 1])
box on; grid on
xlabel('$p$'); ylabel('$\zeta$')
legend('show','location','NorthEast')


% Communication reduction:
fprintf('\n--- COMMUNICATION REDUCTION ---\n')
fprintf('p\t m\t comm. red.\n')
for p = [6 9 12]
    for m = 1:5
        cr = 100*(1-f(m,p));
        fprintf('%d\t %d\t %2.2f%% \n',p,m,cr)
    end
end
fprintf('\n')
for p = [12 15 20]
    for m = [1 3 5 7 9]
        cr = 100*(1-f(m,p));
        fprintf('%d\t %d\t %2.2f%% \n',p,m,cr)
    end
end
fprintf('\n')

end

function update_handle(h,c)
    h.LineStyle = '-';
    h.Marker = 'o';
    h.LineWidth = 0.75;
    h.Color = c;
    h.MarkerSize = 3;
    h.MarkerEdgeColor = c;
    h.MarkerFaceColor = c;
end
