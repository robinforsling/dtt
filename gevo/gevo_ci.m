function [Psi,varargout] = gevo_ci(R1,R2,H,m)
% --- gevo_ci() ----------------------------------------------------------
% The GEVO method for covariance intersection loss function.
%
% 2022-09-29 Robin Forsling

max_iter = 50; J_thres = 0.00001; wmin = 0.01; wmax = 1; % Algorithm settings
I1 = inv(R1);   

% INITIALIZE
w = 0.5; J = Inf;

% ITERATE
for k = 1:max_iter
    J_prev = J;
    Q = H*R1*R1*H'/(w*w);
    S = H*R1*H'/w + R2/(1-w);

    % SOLVE FOR Psi
    [X,D] = eig(Q,S,'qz');
    idx_vec = get_max_idx_vec(D,m);
    Psi = X(:,idx_vec)';

    % SOLVE FOR w
    f = @(w) trace(inv(w*I1 + (1-w)*H'*Psi'/(Psi*R2*Psi')*Psi*H));
    w = fminbnd(f,wmin,wmax);
    J = ci_loss_function(R1,R2,H,Psi,w);

    if (J_prev-J)/J < J_thres; break; end
end

Psi = gram_schmidt_process(Psi);

if k == max_iter; warning('gevopt_ci: reached maximum number of iterations'); end
if nargout > 1; varargout{1} = J; end

end

function J = ci_loss_function(R1,R2,H,Psi,w)
    if w >= 1; J = trace(R1); return;
    elseif w <= 0; error('ci_loss_function: omega out of range')
    end
    J = trace(inv(w*inv(R1) + (1-w)*H'*Psi'/(Psi*R2*Psi')*Psi*H));
end