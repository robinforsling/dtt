function [Psi,varargout] = gevo_bsc(R1,R2,R12,H,m)
% --- gevo_bsc() ----------------------------------------------------------
% The GEVO method for Bar-Shalom-Campo loss function.
%
% 2022-09-29 Robin Forsling

Q = make_symmetric((R1*H'-R12)'*(R1*H'-R12)); 
S = make_symmetric(H*R1*H'+R2-H*R12-R12'*H');
[X,D] = eig(Q,S,'qz');
idx_vec = get_max_idx_vec(D,m);
Psi = gram_schmidt_process(X(:,idx_vec))';

if nargout > 1; varargout{1} = bsc_loss_function(R1,R2,R12,H,Psi); end

end

function J = bsc_loss_function(R1,R2,R12,H,Psi)
    A = Psi*(H*R1-R12');
    B = Psi*(H*R1*H'+R2-H*R12-R12'*H')*Psi';
    J = trace(R1 - A'/B*A);
end