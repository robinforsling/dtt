function [Psi,varargout] = gevo_le(R1,R2,H,m)   
% --- gevo_le() ----------------------------------------------------------
% The GEVO method for the largest ellipsoid method loss function.
%
% 2022-09-29 Robin Forsling

n = size(R1,1); 
[U1,D1] = eig(make_symmetric(inv(R1))); 
T1 = inv(sqrtm(D1))*U1.';
[U2,D2] = eig(make_symmetric(T1*H'/R2*H*T1'));
T = U2'*T1; Ti = inv(T);
Gammai = Ti*diag(min([ones(1,n) ; diag(D2)']))*Ti';
R12 = R1*Gammai*H'*R2;

[Psi,J] = gevo_bsc(R1,R2,R12,H,m); 
if nargout > 1; varargout{1} = J; end