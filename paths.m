function paths(varargin)
% --- paths() -------------------------------------------------------------
% Add or remove path. 
%   Add path:       b = true 
%   Remove path:    b = false
%
% 2022-09-29 Robin Forsling

if nargin > 0; b = varargin{1}; else; b = true; end

if b; addpath(genpath(pwd))
else; rmpath(genpath(pwd))
end