function [xhat,P] = bsc_dimred(y1,R1,yPsi,RPsi,R12Psi,H,Psi)
% --- bsc_dimred() --------------------------------------------------------
% Fusion of (y1,R1) and dimension-reduced estimate (yPsi,RPsi,Psi) using 
% generalization of the Bar-Shalom-Campo formulas.
%
% 2022-09-29 Robin Forsling 

SPsi = Psi*H*R1*H'*Psi'+RPsi-Psi*H*R12Psi-R12Psi'*H'*Psi';
K2 = (R1*H'*Psi'-R12Psi)/SPsi;
K1 = eye(size(R1)) - K2*Psi*H;

xhat = K1*y1 + K2*yPsi;
P = R1 - K2*SPsi*K2';