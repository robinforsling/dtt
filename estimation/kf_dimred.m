function [xhat,P] = kf_dimred(y1,R1,yPsi,RPsi,H,Psi)
% --- kf_dimred() ---------------------------------------------------------
% Fusion of (y1,R1) and dimension-reduced estimate (yPsi,RPsi,Psi) using 
% Kalman fuser.
%
% 2022-09-29 Robin Forsling 

I1 = inv(R1); IPsi = inv(RPsi); 
P = inv(I1 + H'*Psi'*IPsi*Psi*H);
xhat = P*(I1*y1 + H'*Psi'*IPsi*yPsi);