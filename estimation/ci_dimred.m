function [xhat,P] = ci_dimred(y1,R1,yPsi,RPsi,H,Psi)
% --- ci_dimred() ---------------------------------------------------------
% Fusion of (y1,R1) and dimension-reduced estimate (yPsi,RPsi,Psi) using 
% covariance intersection.
%
% 2022-09-29 Robin Forsling 

I1 = inv(R1); IPsi = inv(RPsi); 

f = @(w) trace( inv(w*I1 + (1-w)*H'*Psi'*IPsi*Psi*H) ); 
w = fminbnd(f,0,1,optimset('Display','off'));

P = inv(w*I1 + (1-w)*H'*Psi'*IPsi*Psi*H);
xhat = P*(w*I1*y1 + (1-w)*H'*Psi'*IPsi*yPsi);