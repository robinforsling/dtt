function [xhat,P] = le_dimred(y1,R1,yPsi,RPsi,H,Psi)
% --- le_dimred() ---------------------------------------------------------
% Fusion of (y1,R1) and dimension-reduced estimate (yPsi,RPsi,Psi) using 
% the largest ellipsoid method.
%
% 2022-09-29 Robin Forsling

I1 = inv(R1); i1 = I1*y1;
IPsi = H'*Psi'/RPsi*Psi*H; iPsi = H'*Psi'/RPsi*yPsi;

n = size(R1,1); 
[U1,D1] = eig(make_symmetric(I1)); 
T1 = inv(sqrtm(D1))*U1';
[U2,D2] = eig(make_symmetric(T1*IPsi*T1'));
T = U2'*T1; Ti = inv(T);

j1 = T*i1;
jPsi = T*iPsi;
jhat = zeros(n,1); J = zeros(n);
for i = 1:n
    if D2(i,i) >= 1; jhat(i) = jPsi(i); J(i,i) = D2(i,i);
    else; jhat(i) = j1(i); J(i,i) = 1;
    end
end

P = inv(Ti*J*Ti');
xhat = P*Ti*jhat;