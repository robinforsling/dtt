function c = get_colors_rgb(varargin)
% --- get_colors_rgb() ----------------------------------------------------
% Returns a struct c of colors as normalized RGB values.
%
% 2022-02-14

% INPUT HANDLING 
if nargin > 0; color_package = varargin{1}; else; color_package = 'cb-custom-1'; end

f = 255;

% --- COLOR DEFINITIONS ---------------------------------------------------
switch lower(color_package)

    % MATLAB LINES
    case 'lines'
        l = lines(7);
        c.red      = l(7,:);
        c.green    = l(5,:);
        c.blue     = l(1,:);
        c.yellow   = l(3,:);
        c.purple   = l(4,:);
        c.orange   = l(2,:);
        c.cyan     = l(6,:);

    % COLORBREWER CUSTOM 1
    case {'cb-custom-1'}
        c.blue = [69,117,180]/f;
        c.blue2 = [28,142,192]/f;
        c.red = [215,48,39]/f;
        c.orange = [244,109,67]/f;
        c.green = [26,152,80]/f;
        c.green2 = [49,163,84]/f;
        c.yellow = [254,224,139]/f;
        c.cyan = [102,194,165]/f;
        c.cyan2 = [65,182,196]/f;
        c.purple = [84,39,136]/f;

    % COLORBREWER DIVERGING: SPECTRAL  
    case {'cb-div-spectral'}
        l = [213,62,79; 244,109,67; 253,174,97; 254,224,139; 255,255,191; 230,245,152; 171,221,164; 102,194,165; 50,136,189]/f;
        c.red = l(1,:);
        c.orange = l(2,:);
        c.lightorange = l(3,:);
        c.beige = l(4,:);
        c.lightbeige = l(5,:);
        c.lightgreen = l(6,:);
        c.lightturkos = l(7,:);
        c.turkos = l(8,:);
        c.blue = l(9,:);
    
    % COLORBREWER QUALITATIVE #1: PAIRED
    case {'cb-qual-1', 'cb-qual-paired'}
        l = [166,206,227; 31,120,180; 178,223,138; 51,160,44; 251,154,153; 227,26,28; 253,191,111; 255,127,0; 202,178,214]/f;
        c.lightblue = l(1,:);
        c.blue = l(2,:);
        c.lightgreen = l(3,:);
        c.green = l(4,:);
        c.pink = l(5,:);
        c.red = l(6,:);
        c.lightorange = l(7,:);
        c.orange = l(8,:);
        c.lightpurple = l(9,:);

    otherwise 
        error('get_colors_rgb: unknown input')
end
% -------------------------------------------------------------------------


% --- ADD GRAY SCALE ------------------------------------------------------
c.white = [1 1 1];
c.llllightgray = 0.95*[1 1 1];
c.lllightgray = 0.90*[1 1 1];
c.llightgray = 0.85*[1 1 1];
c.lightgray = 0.75*[1 1 1];
c.gray = 0.5*[1 1 1];
c.darkgray = 0.25*[1 1 1];
c.ddarkgray = 0.15*[1 1 1];
c.dddarkgray = 0.1*[1 1 1];
c.ddddarkgray = 0.05*[1 1 1];
c.black = [0 0 0];
% -------------------------------------------------------------------------




%         c. = l(1,:);
%         c. = l(2,:);
%         c. = l(3,:);
%         c. = l(4,:);
%         c. = l(5,:);
%         c. = l(6,:);
%         c. = l(7,:);
%         c. = l(8,:);
%         c. = l(9,:);