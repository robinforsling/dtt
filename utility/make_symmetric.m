function M = make_symmetric(M)

M = (M+M.')/2;